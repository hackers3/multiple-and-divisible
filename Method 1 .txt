#include <iostream>
using namespace std;

//Compile, link then execute the program.
//Modify the program to compute and output the sum and product of A and B.
//Modify the program to check if the sum is an even or an odd value.
//Modify the program to check if the product is divisible by 5 or not.

int main()
{
    int x = 0;
    int y = 0;
    int sum = 0;
    int product = 0;

    cout << "enter the value of x:";
    cin >> x;

    cout << "enter the value of y:";
    cin >> y;

    sum = x + y;
    product = x * y;

    cout << "The sum is: " << sum << endl;

    if ( sum % 2 == 0) {
        cout << "the sum is even" << endl;
    }

    else {
        cout << "the sum is odd" << endl;
    }

    cout << "The product is: " << product << endl;

    if (product % 5 == 0) {
        cout << "The product is divisible by 5" << endl;
    }

    else {
        cout << "The product is NOT divisible by 5" << endl;
    }

    system("pause");

    return 0;
}